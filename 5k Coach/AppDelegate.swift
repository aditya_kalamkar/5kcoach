//
//  AppDelegate.swift
//  5k Coach
//
//  Created by Aditya Kalamkar on 17/06/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        sleep(2)
        window = UIWindow(frame: UIScreen.main.bounds)
        DBSetup.sharedInstance.createTrackerTable()
        let trackerViewController = TrackerViewController() as UIViewController
        let navigationController = UINavigationController(rootViewController: trackerViewController)
        navigationController.navigationBar.isTranslucent = false
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }

}

