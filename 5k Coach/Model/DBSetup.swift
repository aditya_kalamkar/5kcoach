//
//  DBSetup.swift
//  5k Coach
//
//  Created by Aditya Kalamkar on 18/06/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import Foundation
import SQLite

struct TrackerStructure{
    var tid : Int
    var trackerTime : String
}
class DBSetup{
    static var sharedInstance = DBSetup()
    var connection : Connection!
    var trackingStructure : TrackerStructure?
    //MARK: - Tracker Table
    let trackerTable = Table("tracker")
    let trackerId = Expression<Int>("trackerId")
    let trackingTime = Expression<String>("trackingTime")
    
    private init(){}
    //MARK: - Initialization For DB
    func initializeDB(){
        do{
            let documentDir = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                let fileUrl = documentDir.appendingPathComponent("TrackerDB").appendingPathExtension("sqlite3")
                connection = try Connection(fileUrl.path)
            }catch {
                print(error)
            }
    }
    
    //MARK: - Creation of DB
    func createTrackerTable(){
        initializeDB()
        do{
            let createTable = self.trackerTable.create{(table) in
                table.column(self.trackerId,primaryKey: .autoincrement)
                table.column(self.trackingTime, unique : true)
            }
            try self.connection.run(createTable)
        }catch{
            print(error)
        }
    }
    
    //MARK: - Insert Query for the DB table
    func insertTrackingData(trackTiming: String){
        let insertTrackingData = self.trackerTable.insert(self.trackingTime <- trackTiming)
        do{
            try self.connection.run(insertTrackingData)
        }catch{
            print(error)
        }
    }
    
    //MARK: - Show Table Values
    func showTrackerEntry(){
        do{
            let trackTiming = try self.connection.prepare(self.trackerTable)
            for i in trackTiming{
                print("Row from Tracker table: \n Tracker id: \(i[self.trackerId]) \n Tracking TIme: \(i[trackingTime])\n")
            }
        }catch{
            print(error)
        }
    }
    
    //MARK: - Filtering Best time from Table
    func historyTime() -> [TrackerStructure]{
         var historyData: [TrackerStructure]  = []
        do{
            let data = try self.connection.prepare(self.trackerTable)
            for d in data{
                historyData.append(TrackerStructure(tid: d[trackerId], trackerTime: d[trackingTime]))
            }
        }catch{
            print(error)
        }
        return historyData
    }
}

