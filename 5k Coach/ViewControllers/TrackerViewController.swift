//
//  TrackerViewController.swift
//  5k Coach
//
//  Created by Aditya Kalamkar on 17/06/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

class TrackerViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var startTime : Double = 0
    var elapsed : Double = 0
    var time : Double = 0
    var milliSec : Double = 0
    var timer = Timer()
    var laps : [String] = []
    var isPlaying = false
    var addLap : Bool = false
    var stopWatchString : String = ""
    
    lazy var historyButton : UIButton = {
        let bound = UIScreen.main.bounds
        let button = UIButton()
        //Core Graphics CGRect method for Button Creation
        button.frame = CGRect(x: bound.origin.x + 5, y: 5, width: 70, height: 15)
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("history", for: .normal)
        button.addTarget(self, action: #selector(historyClicked), for: .touchUpInside)
        return button
    }()
    
    lazy var delimiterLabel1 : UILabel = {
        let label = UILabel()
        label.text  = ":"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalToConstant: 15).isActive = true
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 30)
        return label
    }()
    
    lazy var delimiterLabel2 : UILabel = {
        let label = UILabel()
        label.text  = ":"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalToConstant: 15).isActive = true
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 30)
        return label
    }()
    
    lazy var stopWatchLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tracker"
        label.font = UIFont.systemFont(ofSize: 20)
        label.textAlignment = .center
        return label
    }()
    
    lazy var stopWatchView : UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        stackView.distribution = UIStackView.Distribution.fill
        stackView.axis = NSLayoutConstraint.Axis.horizontal
        return stackView
    }()
    
    lazy var stopWatchTimerMinutesLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00"
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = .right
        return label
    }()
    
    lazy var stopWatchTimerSecondsLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00"
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = .center
        return label
    }()
    
    lazy var stopWatchTimerMillisecondsLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00"
        label.font = UIFont.systemFont(ofSize: 30)
        label.textAlignment = .left
        return label
    }()
    
    lazy var startButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .white
        button.setTitle("Start", for: .normal)
        button.layer.cornerRadius = 15.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(startStopWatch), for: .touchUpInside)
        return button
    }()
    
    lazy var lapsButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .white
        button.setTitle("Laps", for: .normal)
        button.layer.cornerRadius = 15.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(lapsStopWatch), for: .touchUpInside)
        return button
    }()
    
    lazy var resetButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .white
        button.setTitle("Reset", for: .normal)
        button.layer.cornerRadius = 15.0
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(resetStopWatch), for: .touchUpInside)
        return button
    }()
    
    lazy var lapsTableView : UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

    //MARK: - Setup All Views
    func setupView(){
        self.view.backgroundColor = .white
        self.view.addSubview(historyButton)
        self.view.addSubview(stopWatchLabel)
        self.stopWatchView.addArrangedSubview(stopWatchTimerMinutesLabel)
        self.stopWatchView.addArrangedSubview(delimiterLabel1)
        self.stopWatchView.addArrangedSubview(stopWatchTimerSecondsLabel)
        self.stopWatchView.addArrangedSubview(delimiterLabel2)
        self.stopWatchView.addArrangedSubview(stopWatchTimerMillisecondsLabel)
        self.view.addSubview(stopWatchView)
        self.view.addSubview(startButton)
        self.view.addSubview(lapsButton)
        self.view.addSubview(resetButton)
        self.lapsTableView.isHidden = true
        self.lapsTableView.dataSource = self
        self.lapsTableView.delegate = self
        self.lapsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(lapsTableView)
        
        //stopWatchLabel Constraint
        view.addConstraint(NSLayoutConstraint(item: stopWatchLabel, attribute: .bottom, relatedBy: .equal, toItem: stopWatchView, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stopWatchLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stopWatchLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 50))
        
        //stopWatchTimerLabel Constraint
        stopWatchView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        view.addConstraint(NSLayoutConstraint(item: stopWatchView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 30))
         view.addConstraint(NSLayoutConstraint(item: stopWatchView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -30))
         stopWatchView.addConstraint(NSLayoutConstraint(item: stopWatchTimerMinutesLabel, attribute: .trailing, relatedBy: .equal, toItem: self.delimiterLabel1, attribute: .leading, multiplier: 1, constant: 0))
         stopWatchView.addConstraint(NSLayoutConstraint(item: stopWatchTimerSecondsLabel, attribute: .leading, relatedBy: .equal, toItem: self.delimiterLabel1, attribute: .trailing, multiplier: 1, constant: 0))
         stopWatchView.addConstraint(NSLayoutConstraint(item: stopWatchTimerSecondsLabel, attribute: .trailing, relatedBy: .equal, toItem: self.delimiterLabel2, attribute: .leading, multiplier: 1, constant: 0))
         stopWatchView.addConstraint(NSLayoutConstraint(item: stopWatchTimerMillisecondsLabel, attribute: .leading, relatedBy: .equal, toItem: self.delimiterLabel2, attribute: .trailing, multiplier: 1, constant: 0))
        stopWatchView.addConstraint(NSLayoutConstraint(item: stopWatchTimerSecondsLabel, attribute: .centerX, relatedBy: .equal, toItem: stopWatchView, attribute: .centerX, multiplier: 1, constant: 0))
        
        //start button Constarint
        view.addConstraint(NSLayoutConstraint(item: startButton, attribute: .top, relatedBy: .equal, toItem: self.stopWatchView, attribute: .bottom, multiplier: 1, constant: 30))
        view.addConstraint(NSLayoutConstraint(item: startButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 30))
        view.addConstraint(NSLayoutConstraint(item: startButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -30))
        
        //laps Button Constraint
        view.addConstraint(NSLayoutConstraint(item: lapsButton, attribute: .top, relatedBy: .equal, toItem: startButton, attribute: .bottom, multiplier: 1, constant: 10))
        view.addConstraint(NSLayoutConstraint(item: lapsButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 30))
        view.addConstraint(NSLayoutConstraint(item: lapsButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -30))
        
        //resetButton Constraint
        view.addConstraint(NSLayoutConstraint(item: resetButton, attribute: .top, relatedBy: .equal, toItem: self.lapsButton, attribute: .bottom, multiplier: 1, constant: 10))
        view.addConstraint(NSLayoutConstraint(item: resetButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 30))
        view.addConstraint(NSLayoutConstraint(item: resetButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -30))
        
        //lapsTableView Constraint
        self.lapsTableView.topAnchor.constraint(equalTo: resetButton.bottomAnchor, constant: 30).isActive = true
        self.lapsTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.lapsTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.lapsTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    //MARK: - StopWatch Actions
    @objc func startStopWatch(){
        print("Started Stop Watch")
       if(isPlaying){
            stop()
            startButton.setTitle("Start", for: .normal)
            resetButton.isEnabled = true
            addLap = false
        }else {
            start()
            startButton.setTitle("Stop", for: .normal)
            resetButton.isEnabled = false
            addLap = true
        }
    }
    
    @objc func lapsStopWatch(){
        print("Laps")
        self.lapsTableView.isHidden = false
        if addLap == true {
            self.laps.insert(stopWatchString, at: 0)
            self.lapsTableView.reloadData()
        } else {
            addLap = false
        }
    }
    
    @objc func resetStopWatch(){
        print("Reseted Stop Watch \(milliSec)")
        timer.invalidate()
        startTime = 0
        time = 0
        elapsed = 0
        isPlaying = false
        let str = String("00")
        stopWatchTimerMinutesLabel.text = str
        stopWatchTimerSecondsLabel.text = str
        stopWatchTimerMillisecondsLabel.text = str
        laps.removeAll(keepingCapacity: false)
        lapsTableView.reloadData()
    }
    
    @objc func historyClicked(){
        let historyVC = HistoryViewController()
        self.navigationController?.pushViewController(historyVC, animated: true)
    }
    
    //MARK: - Stopwatch Flow Methods
    func start(){
        startTime = Date().timeIntervalSinceReferenceDate - elapsed
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        isPlaying = true
    }
    func stop(){
        elapsed = Date().timeIntervalSinceReferenceDate - startTime
        milliSec = Date().timeIntervalSinceReferenceDate - startTime
        timer.invalidate()
        isPlaying = false
    }
    
    @objc func updateCounter(){
        time = Date().timeIntervalSinceReferenceDate - startTime
        milliSec = Date().timeIntervalSinceReferenceDate - startTime
        let minutes = UInt8(time / 60.0)
        time -= (TimeInterval(minutes) * 60)
        
        let seconds = UInt8(time)
        time -= TimeInterval(seconds)
        
        let milliseconds = UInt8(time * 60)
        
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let strMilliseconds = String(format: "%02d", milliseconds)
        
        stopWatchString = "\(strMinutes):\(strSeconds):\(strMilliseconds)"
        stopWatchTimerMinutesLabel.text = strMinutes
        stopWatchTimerSecondsLabel.text = strSeconds
        stopWatchTimerMillisecondsLabel.text = strMilliseconds
    }
    
    //Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laps.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = "Lap \(laps.count - indexPath.row)"
        cell.detailTextLabel?.text = laps[indexPath.row]
        DBSetup.sharedInstance.insertTrackingData(trackTiming: laps[indexPath.row])
        return cell
   }
       
}
