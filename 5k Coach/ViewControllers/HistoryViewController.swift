//
//  HistoryViewController.swift
//  5k Coach
//
//  Created by Aditya Kalamkar on 21/06/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    var historyStruct : [TrackerStructure] = []
    var trackId : [String] = []
    var trackingTime : [String] = []
    
    lazy var historyLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "History"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 30)
        return label
    }()
    
    lazy var historyTableView : UITableView = {
       let tableView = UITableView()
       tableView.translatesAutoresizingMaskIntoConstraints = false
       return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.historyTableView.dataSource = self
        self.historyTableView.delegate = self
        self.historyTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(historyLabel)
        self.view.addSubview(historyTableView)
        historyStruct = DBSetup.sharedInstance.historyTime()
        
        for i in historyStruct{
            trackId.append(String(i.tid))
            trackingTime.append(i.trackerTime)
        }
        self.historyTableView.reloadData()
        
        //History label Constraint
        view.addConstraint(NSLayoutConstraint(item: historyLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: historyLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 10))
        
        //historyTableView Constraint
        self.historyTableView.topAnchor.constraint(equalTo: self.historyLabel.bottomAnchor, constant: 30).isActive = true
        self.historyTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.historyTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.historyTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyStruct.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = trackId[indexPath.row]
        cell.detailTextLabel?.text = trackingTime[indexPath.row]
        return cell
    }
}
